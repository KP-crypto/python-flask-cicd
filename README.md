# PROJECT NAME

![Project Stage][project-stage-shield]
![Maintenance][maintenance-shield]
![Pipeline Status][pipeline-status]


[![License][license-shield]](LICENSE.md)
[![Maintaner][maintainer-shield-james]](https://gitlab.com/jsandlin)
[![pipeline status]([pipeline-status])](https://gitlab.com/sandlin/examples/python_flask/-/commits/groot)
[![coverage report]([code-coverage])](https://gitlab.com/sandlin/examples/python_flask/-/commits/groot)

## Table of Contents
[[_TOC_]]

####

<!-- Let's define some variables for ease of use. -->
[PROJECT_SUBURL]: sandlin/examples/python_flask
[project-stage-shield]: https://img.shields.io/badge/project%20stage-development-yellowgreen.svg
[issue]: https://gitlab.com/sandlin/examples/python_flask/issues
[license-shield]: https://img.shields.io/github/license/hassio-addons/repository-beta.svg
[maintenance-shield]: https://img.shields.io/maintenance/yes/2021.svg
[maintainer-shield-james]: https://img.shields.io/badge/maintainer-James_Sandlin-blueviolet
[maintainer-shield-novi]: https://img.shields.io/badge/maintainer-Novi_Sandlin-blueviolet
[pipeline-status]: https://gitlab.com/sandlin/examples/python_flask/badges/groot/pipeline.svg
[code-coverage]: https://gitlab.com/sandlin/examples/python_flask/badges/groot/coverage.svg
